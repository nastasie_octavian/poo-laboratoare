
public interface Observer {
	public void signal(String msg);
}
