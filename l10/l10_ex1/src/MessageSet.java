import java.util.*;

public class MessageSet {
	private LinkedList<String> messages = new LinkedList<String>();
	private LinkedList<Observer1> observers = new LinkedList<Observer1>();
	
	public void attach(Observer1 o) {
		observers.add(o);
	}
	
	public void addMessage(String m) {
		messages.add(m);
		Iterator<Observer1> it = observers.iterator();
		while(it.hasNext()) {
			it.next().signal(m);
		}
	}

}
