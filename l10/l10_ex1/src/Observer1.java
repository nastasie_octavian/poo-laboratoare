import java.util.*;

public class Observer1 implements Observer {
	private static int count = 0;
	private int id;
	public Observer1() {
		id = ++count;
	}
	public void signal(String message) {
		System.out.println("Observer " + id + ": " + message);
	}
}

class Speaker extends Observer1 {
	protected HashMap<String, String> dict = new HashMap<String, String>();
	public void signal(String message) {
		super.signal(message);
		
		String[] aux = message.split(" ");
		String translated = "";
		for(int i = 0; i < aux.length; i++) {
			String value = dict.get(aux[i]);
			if(value !=null)
					aux[i] = value;
			
			translated+=aux[i] + " ";
		}
		
		System.out.println("Mesaj tradus: " + translated);
	}
}

class POOSpeaker extends Speaker {
	public POOSpeaker() {
		super();
		dict.put("tema", "horror");
	}
}

class PaulSpeaker extends Speaker {
	public PaulSpeaker() {
		super();
		dict.put("Paul", "Unul");
	}
}
