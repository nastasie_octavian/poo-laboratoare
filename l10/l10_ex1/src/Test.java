
public class Test {

	public static void main(String[] args) {
		MessageSet msg = new MessageSet();
		msg.attach(new Observer1());
		msg.attach(new POOSpeaker());
		msg.attach(new PaulSpeaker());
		msg.addMessage("Paul :P");
		msg.addMessage("POO tema 3 :P");
	}

}
