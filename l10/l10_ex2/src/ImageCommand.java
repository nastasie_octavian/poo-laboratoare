
public abstract class ImageCommand {

	public abstract void execute();
	public abstract void Undo();
	public abstract void Redo();

}

class ResizeCommand extends ImageCommand {
	private Image img;
	
	public ResizeCommand(Image img) {
		this.img = img;
	}
	
	public void execute() {
		img.Resize();
		img.action();
	}

	@Override
	public void Undo() {
		img.UndoResize();
		img.action();
		
	}

	@Override
	public void Redo() {
		execute();
		
	}
}

class CropCommand extends ImageCommand {
	private Image img;
	
	public CropCommand(Image img) {
		this.img = img;
	}
	
	public void execute() {
		img.Crop();
		img.action();
	}

	@Override
	public void Undo() {
		img.UndoCrop();
		img.action();
		
	}

	@Override
	public void Redo() {
		execute();
		
	}
}

class BlurCommand extends ImageCommand {
	private Image img;
	
	public BlurCommand(Image img) {
		this.img = img;
	}
	
	public void execute() {
		img.ApplyFilter();
		img.action();
	}

	@Override
	public void Undo() {
		img.UndoApplyFilter();
		img.action();
		
	}

	@Override
	public void Redo() {
		execute();
		
	}
}
