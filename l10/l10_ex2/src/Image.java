
public class Image {
	private int w = 100;
	private int h = 50;
	private int cropped = 0;
	private boolean BlurFilter = false;
	
	public void Display() {
		System.out.println("Dimensiuni: " + w +"X" +h + " Cropped: " + cropped + " Filter: " + BlurFilter);
	}
	public void Resize() {
		w*=2;
		h*=2;
	}
	
	public void UndoResize() {
		w/=2;
		h/=2;
	}
	
	public void Crop() {
		cropped++;
	}
	
	public void UndoCrop() {
		cropped--;
	}
	
	public void ApplyFilter() {
		BlurFilter = true; 
	}
	
	public void UndoApplyFilter()  {
		BlurFilter = false;
	}
	
	public void action() {
		Display();
	}
}
