import java.util.*;

class Invoker {
	private static Stack<ImageCommand> undo = new Stack<ImageCommand>();
	private static Stack<ImageCommand> redo = new Stack<ImageCommand>();
	
	public static void EditImage(ImageCommand cmd) {
		cmd.execute();
		undo.push(cmd);
	}
	public static void Undo() {
		ImageCommand cmd = undo.pop();
		cmd.Undo();
		redo.push(cmd);
	}
	
	public static void Redo() {
		ImageCommand cmd = redo.pop();
		cmd.Redo();
		undo.push(cmd);
	}
}

public class Test {

	public static void main(String[] args) {
		Image img = new Image();
		img.Display();
		
		ImageCommand ic = ImageCommandFactory.getComand(img, CommandType.RESIZE);
		Invoker.EditImage(ic);
		ImageCommand ic3 = ImageCommandFactory.getComand(img, CommandType.CROP);
		Invoker.EditImage(ic3);
		ImageCommand ic4 = ImageCommandFactory.getComand(img, CommandType.FILTER);
		Invoker.EditImage(ic4);
		ImageCommand ic2 = ImageCommandFactory.getComand(img, CommandType.RESIZE);
		Invoker.EditImage(ic2);
		
		System.out.println("Undo");
		Invoker.Undo();
		System.out.println("Undo");
		Invoker.Undo();
		System.out.println("Redo");
		Invoker.Redo();	
		System.out.println("Final state");
		img.Display();
	}

}
