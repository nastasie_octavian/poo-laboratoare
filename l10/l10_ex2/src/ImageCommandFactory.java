
public class ImageCommandFactory {
	public static ImageCommand getComand(Image img, CommandType type){
		switch(type) {
			case RESIZE:
				return new ResizeCommand(img);
			case CROP:
				return new CropCommand(img);
			default:
				return new BlurCommand(img);
		}
	}

}
