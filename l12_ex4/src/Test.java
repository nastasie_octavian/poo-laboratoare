import java.util.*;

class MyVector3 implements Sumabil {
	int X = 0, Y = 0, Z = 0;

	public MyVector3(int a, int b, int c) {
		X = a;
		Y = b;
		Z = c;
	}
	@Override
	public void addValue(Sumabil value) {
		
		X += ((MyVector3)value).X;
		Y += ((MyVector3)value).Y;
		Z += ((MyVector3)value).Z;
	}
	
	public String toString() {
		return X + " " + Y + " " + Z;
	}
}

class MyMatrix implements Sumabil {
	int[][] matr = new int[4][4];
	
	public MyMatrix(int t) {
		for(int i = 0; i < 4; i++)
			for(int j = 0; j <4; j++)
				matr[i][j] = i*j*t;
	}

	@Override
	public void addValue(Sumabil value) {
		for(int i = 0; i < 4; i++)
			for(int j = 0; j <4; j++)
				matr[i][j] += ((MyMatrix)value).matr[i][j];
	}
	
	public String toString() {
		String aux = "";
		for(int i = 0; i < 4; i++)
				aux +=Arrays.toString(matr[i]) + "\n"; 
		return aux;
	}
}

public class Test {
	static <T extends Sumabil> T addAll(Collection<Sumabil> c) {
		Iterator<T> it = (Iterator<T>) c.iterator();
		boolean matr = false;
		T aux = null;
		if(it.hasNext()) {
			aux = it.next();
			if(aux instanceof MyMatrix)
				matr = true;
			while(it.hasNext()) {
				T aux2 = it.next();
					if((aux2 instanceof MyMatrix && matr) || (!(aux2 instanceof MyMatrix) && !matr))
				aux.addValue(aux2);
			}
		}
		return aux;
		
	}
	public static void main(String[] args) {
		MyVector3 m1 = new MyVector3(1, 2, 3);
		MyVector3 m2 = new MyVector3(2, 1, 7);
		m1.addValue(m2);
		System.out.println(m1);
		
		MyMatrix mat1 =  new MyMatrix(1);
		MyMatrix mat2 =  new MyMatrix(2);
		mat1.addValue(mat2);
		System.out.println(mat1);
		
		List<Sumabil> sum = new LinkedList(Arrays.asList(
				new Sumabil[] { new MyVector3(1, 2, 3), new MyVector3(2, 3, 4), new MyVector3(3, 4, 5)}));
		System.out.println(addAll(sum));
		
		List<Sumabil> sum2 = new LinkedList(Arrays.asList(
				new MyMatrix(1), new MyMatrix(1), new MyMatrix(2), new MyMatrix(2)));
		System.out.println(addAll(sum2));
		
		sum2 = new LinkedList(Arrays.asList(
				new MyMatrix(1), new MyVector3(1, 2, 3), new MyVector3(4, 5 ,6), new MyMatrix(2)));
		System.out.println(addAll(sum2));
		
		sum2 = new LinkedList(Arrays.asList(
				new MyVector3(1, 2, 3), new MyMatrix(1), new MyVector3(4, 5 ,6), new MyMatrix(2)));
		System.out.println(addAll(sum2));
	}

}
