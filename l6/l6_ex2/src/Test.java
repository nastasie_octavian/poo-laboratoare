
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Encrypter one = EncryptorFactory.get();
		Encrypter two = EncryptorFactory.get();
		
		String cleartext = "POO";
		String ciphertext = one.encrypt(cleartext);
		System.out.println(ciphertext+" = " + one.decrypt(ciphertext)+ " =? "+ two.decrypt(ciphertext));
		
		ciphertext = two.encrypt(cleartext);
		System.out.println(ciphertext+" = " + two.decrypt(ciphertext)+ " =? "+ one.decrypt(ciphertext));
	}

}
