import java.util.*;

public class EncryptorFactory {
	static class Enc1 implements Encrypter {

		@Override
		public String encrypt(String cleartext) {
			char[] aux = cleartext.toCharArray();
			for(int i = 0; i < aux.length; i++)
				aux[i] = (char)((int)aux[i] + 1);
			return new String(aux);
		}

		@Override
		public String decrypt(String cipher) {
			char[] aux = cipher.toCharArray();
			for(int i = 0; i < aux.length; i++)
				aux[i] = (char)((int)aux[i] - 1);
			return new String(aux);
		}
		
	}
	
	static class Enc2 implements Encrypter {

		@Override
		public String encrypt(String cleartext) {
			char[] aux = cleartext.toCharArray();
			for(int i = 0; i < aux.length; i++)
				aux[i] = (char)((int)aux[i] - 5);
			return new String(aux);
		}

		@Override
		public String decrypt(String cipher) {
			char[] aux = cipher.toCharArray();
			for(int i = 0; i < aux.length; i++)
				aux[i] = (char)((int)aux[i] + 5);
			return new String(aux);
		}
		
	}
	private static Random rand = new Random();
	
	public static Encrypter get() {
		if(rand.nextBoolean()) 
			return new Enc1();
		else
			return new Enc2();
	}

}
