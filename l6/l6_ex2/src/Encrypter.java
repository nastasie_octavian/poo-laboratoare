
public interface Encrypter {

	public String encrypt(String cleartext);
	public String decrypt(String cipher);
}
