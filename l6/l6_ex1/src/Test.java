import java.util.*;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MyArrayList list = new MyArrayList(10);
		Random rand = new Random();
		for(int i = 0; i < 5; i++)
			list.add(1 + rand.nextInt(100));
		System.out.println(list);
		Iterator iter = list.iterator();
		while(iter.hasNext())
			System.out.println(iter.next());
		System.out.println(iter.next());
		
		list = new MyArrayList(10);
		for(int i = 0; i < 5; i++)
			list.add(1 + rand.nextInt());
		
		iter = list.iterator();
		list.add(13);
		System.out.println(iter.next());
	}

}
