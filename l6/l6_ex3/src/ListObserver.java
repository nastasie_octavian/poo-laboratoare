
public interface ListObserver {
	public void elementAdded(ListEvent e);
	public void elementRemoved(ListEvent e);
	public MyArrayList getlist();
}
