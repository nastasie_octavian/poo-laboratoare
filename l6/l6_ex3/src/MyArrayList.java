import java.util.*;

public class MyArrayList implements Observed {
	private LinkedList<ListObserver> listObservers = new LinkedList<ListObserver>();
	
	
	private class IteratorMy implements Iterator {

		private int poz = 0;
		@Override
		
		public boolean hasNext() {
			return poz < filled;
		}

		@Override
		public int next() {
			if(modifAfterIter)
				return -1;
			if(poz == filled)
				return 0;
			return vals[poz++];
		}
		
	}
	
	public Iterator iterator() {
		modifAfterIter = false;
		return new IteratorMy();
	}
	
	int[] vals;
	int filled = 0;
	private boolean modifAfterIter;
	
	public MyArrayList() {
		vals = new int[10];
	}
	
	public MyArrayList(int size) {
		vals = new int[size];
	}
	
	public void add(final int value) {
		
		modifAfterIter = true;
		long startTime = System.nanoTime();
		if(filled >= vals.length) {
			int[] aux = new int[2*vals.length];
			for(int i = 0; i < vals.length; i++)
					aux[i] = vals[i];
			vals = aux;
			
		}
		vals[filled++] = value;
		
		final long duration = System.nanoTime() - startTime;;
		for(int j = 0; j < listObservers.size(); j++)
			listObservers.get(j).elementAdded(new ListEvent() {
				private int elem = value;
				private long time = duration;
				@Override
				public MyArrayList getList() {
					return MyArrayList.this;
				}

				@Override
				public int getElement() {
					return elem;
				}

				@Override
				public long getDuration() {
					return time;
				}
			
			});
	}
	
	public boolean contains(int value) {
		for(int i = 0; i < filled; i++)
			if(vals[i] == value)
				return true;
		return false;
	}
	
	public void remove(int index) {
		if(index < 0 || index >= filled)
			System.out.println("Eroare !!! Out of bounds!");
		else {
			long startTime = System.nanoTime();
			modifAfterIter = true;
			final int value = vals[index];
			for(int i = index; i < filled-1; i++)
				vals[i] = vals[i+1];  
			filled--;
			
			final long duration = System.nanoTime() - startTime;;
			for(int j = 0; j < listObservers.size(); j++)
				listObservers.get(j).elementRemoved(new ListEvent() {
					private int elem = value;
					private long time = duration;
					@Override
					public MyArrayList getList() {
						return MyArrayList.this;
					}

					@Override
					public int getElement() {
						return elem;
					}

					@Override
					public long getDuration() {
						return time;
					}
				
				});
		}
	}
	
	public float get(int index) {
		if(index < 0 || index >= filled) {
			System.out.println("Eroare !!! Out of bounds!");
			return -1;
		}
		else
			return vals[index];
	}
	
	public int size() {
		return filled;
	}
	
	public String toString() {
		String s = new String("[ ");
		for(int i=0; i < filled; i++)
			s+= " " + String.valueOf(vals[i]);
		s+= " ]";
		return s;
	}

	@Override
	public void registerObserver(ListObserver o) {
		listObservers.add(o);
		
	}
}
