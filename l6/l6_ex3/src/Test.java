import java.util.Random;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MyArrayList list = new MyArrayList(10);
		
		list.registerObserver(new ListObserver() {
			
			@Override
			public void elementAdded(ListEvent e) {
				System.out.println("Observer One: Adaugare in lista: " + e.getClass() + " element: "
									+ e.getElement() + " durata: " + e.getDuration());
			}

			@Override
			public void elementRemoved(ListEvent e) {
				System.out.println("Observer One: Stergere din lista: " + e.getClass() + " element: "
						+ e.getElement() + " durata: " + e.getDuration());
			}

			@Override
			public MyArrayList getlist() {  return null; }
			
		});
		
		ListObserver observer = new ListObserver() {
			MyArrayList aux = new MyArrayList(10);
			@Override
			public void elementAdded(ListEvent e) { }

			@Override
			public void elementRemoved(ListEvent e) {
				if(e.getDuration() > 1500) {
					aux.add(e.getElement());
				}
			}

			@Override
			public MyArrayList getlist() {
				return aux;
				
			}
			
		};
		
		MyArrayList listPar = observer.getlist();
		list.registerObserver(observer);
		
		
		Random rand = new Random();
		for(int i = 0; i < 5; i++)
			list.add(1 + rand.nextInt(100));
		while( list.size() > 0)
			list.remove(0);
		
		for(int i = 0; i < listPar.size(); i++)
			System.out.println(listPar.get(i));
			
	}

}
