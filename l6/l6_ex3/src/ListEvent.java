
public interface ListEvent {
	public MyArrayList getList();
	public int getElement();
	public long getDuration();
}
