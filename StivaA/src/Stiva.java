
public class Stiva {
	private Array a;
	int poz = 0;
	
	public Stiva(int n) {
		a = new Array(n);
	}
	
	public int push(int val) {
		int rez = a.set(poz, val);
		if(rez != Array.ERROR)
			poz++;
		return rez;
	}
	
	public int pop() {
		int rez = a.get(--poz);	
		return rez;
	}
}
