
public class Test {

	/**
	 * @param args
	 */
	public static void showUsedMemory() {
	    long usedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	    System.out.println(usedMem);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		long start = System.nanoTime();
		new Integer(2+3);
		long rez = System.nanoTime() - start;
		System.out.println("Integer(2+3): " + rez);
		

		start = System.nanoTime();
		int a = 2 + 3;
		rez = System.nanoTime() - start;
		System.out.println("2+3: " + rez);
		
		String[] s1 = new String[50000];
		showUsedMemory();
		for(int i=0; i < s1.length; i++)
				s1[i] = "abc";
		showUsedMemory();
		for(int i=0; i < s1.length; i++)
			s1[i] = new String("abc");
		showUsedMemory();
	}
}
