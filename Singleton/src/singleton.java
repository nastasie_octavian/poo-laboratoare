
public class singleton {
	private static singleton sing = null;
	
	private singleton() { }
	private singleton(int val) { 
		this.val =val;  
	}
	
	int val = 0;
	
	public static singleton getInstance() {
		if(sing == null)
			sing = new singleton();
		return sing;
	}
	
	public static void createInstance(int val) {
		if(sing == null)
			sing = new singleton(val);
		sing.val = val;
	}
}
