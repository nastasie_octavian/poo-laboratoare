
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		singleton sig = singleton.getInstance();
		System.out.println("Val "+ sig.val);
		sig.val = 13;
		
		// singleton test =  new singleton();
		singleton sig2 = singleton.getInstance();
		System.out.println("Val "+ sig2.val);
		
		sig2.val = 35;
		singleton sig3 = singleton.getInstance();
		System.out.println("Val "+ sig3.val);
		
		singleton.createInstance(27);
		System.out.println("Val "+ sig3.val);
		
		sig3 = singleton.getInstance();
		System.out.println("Val "+ sig3.val);
	}
}
