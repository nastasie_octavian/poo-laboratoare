
public class PasswordMaker {
	static final int MAGIC_NUMBER = 13;
	String pass;
	
	public PasswordMaker(String firstName, String lastName, int age) {
		RandomStringGenerator rsg = new RandomStringGenerator(MAGIC_NUMBER, "];],.<>:+_)(*&^%$#@!");
		pass = new String();
		pass += firstName.substring(firstName.length() - (age % 3));
		pass += rsg.next();
		pass += String.valueOf( age +lastName.length());
	}
	
	public String getPassword() {
		return pass;
	}
}
