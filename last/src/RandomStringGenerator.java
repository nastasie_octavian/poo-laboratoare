import java.util.Random;

public class RandomStringGenerator {
		Random generator;
		int length;
		char[] alpha;
		
		public RandomStringGenerator(int length,String s ) {
			generator = new Random();
			this.length = length;
			alpha = s.toCharArray();
		}
		
		public String next() {
			String s = new String("");
			if(alpha.length > 0)
				for(int i = 0; i < length; i++) {
						s += alpha[generator.nextInt(alpha.length)];
				}
			return s;
		}
}
