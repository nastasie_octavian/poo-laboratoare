
public class Student extends Persoana {
	protected int nota;

	public Student() { }
	public Student(String nume, int nota) {
		super(nume);
		this.nota = nota;
	}
	
	public String toString() {
		return "Student: " + super.toString() + ", " + nota;
	}
	
	public boolean equals(Object s) {
		return nume.equals(((Student)s).nume) &&  nota == ((Student)s).nota;
	}
	
	public void invata() {
		System.out.println(nume + " invata");
	}
}
