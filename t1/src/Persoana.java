
public class Persoana {
	protected String nume;
	
	public Persoana() { }
	
	public Persoana(String nume) { 
		this.nume = nume;
	}
	
	public String toString() {
		return nume;
	}
}
