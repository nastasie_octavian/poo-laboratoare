import java.io.*;

public class Wc {
	public static void main(String[] args) {
		if(args.length < 1)
			throw new RuntimeException("Este necesar cel putin un parametru: fisierul analizat");
		
		BufferedReader in= null;
		int wordcount = 0, linecount = 0;
		String file;
		
		if(args.length == 2)
			file = args[1];
		else
			file = args[0];
		
		try {
			in = new BufferedReader(new FileReader(file));
			String line;
			
			// cat timp mai exista o linie in fisier
			while( (line =in.readLine()) != null) {
				linecount++; // contorizez nr de linii
				wordcount += line.split(" ").length; // actualizez nr de cuvinte 
				// cu nr de cuvinte despartite prin " "
			}
			
			// daca este pasat -l afisez nr de linii
			if(args[0].compareTo("-l") == 0) 
				System.out.println("Nr linii: " + linecount);
			else
				System.out.println("Nr cuvine: " + wordcount);
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(in !=null) {
				try {
					in.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
