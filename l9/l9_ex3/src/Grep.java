import java.io.*;

public class Grep {
	public static void main(String[] args) {
		if(args.length < 2)
			throw new RuntimeException("Syntaxa de apelare java Grep <sursa> <cuvant>");
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(args[0]));
			int linecount = 0;
			String line;
			
			// cat timp nu am terminat de parcurs fisierul
			while((line = in.readLine()) != null) {
				linecount++;
				// verific daca cuvantul cautat este in linia curenta
				if(line.contains(args[1]))
					System.out.println(linecount+": " + line);
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(in != null) {
				try {
					in.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
