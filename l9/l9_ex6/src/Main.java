import java.io.*;

public class Main {

	public static void main(String[] args) {
		DataInputStream inp = null;
		try {
			
			// citesc offsetul
			inp = new DataInputStream(new FileInputStream("secret.txt"));
			long pozb = inp.readLong();
			// scad din offset nr de bytes dintr-un long si skip la rest
			inp.skip(pozb - 8);
			
			// citesc numarul secret
			System.out.println("Numarul este: " + inp.readInt());
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(inp != null) {
				try {
					inp.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
