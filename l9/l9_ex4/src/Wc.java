import java.io.*;

public class Wc {
	public static void main(String[] args) {
		if(args.length < 1)
			throw new RuntimeException("Este necesar cel putin un parametru: fisierul analizat");
		
		BufferedReader in= null;
		int wordcount = 0, linecount = 0;
		String file;
		
		if(args.length == 2)
			file = args[1];
		else
			file = args[0];
		
		try {
			// ma folosesc de InputStreamReader ca sa "leg" lucrul cu biti cu lucrul cu caractere
			in = new BufferedReader(new InputStreamReader(new InputDecrypt(new FileInputStream(file))));
			String line;
			
			while( (line =in.readLine()) != null) {
				linecount++;
				wordcount += line.split(" ").length;
			}
			
			if(args[0].compareTo("-l") == 0) 
				System.out.println("Nr linii: " + linecount);
			else
				System.out.println("Nr cuvine: " + wordcount);
			
		} catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			if(in !=null) {
				try {
					in.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
