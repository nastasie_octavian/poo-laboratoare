import java.io.*;

public class OutputEncrypt extends FilterOutputStream {

	public OutputEncrypt(OutputStream out) {
		super(out);
	}
	
	@Override
	public void write(int b) throws IOException {
		super.write(b+1);
	}
}
