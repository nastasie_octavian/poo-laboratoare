import java.io.*;

public class Cp {

	public static void main(String[] args) {
		if(args.length < 2)
			throw new RuntimeException("Sunt necesari 2 parametrii: sursa si destinatie");
		
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(args[0]));
			out = new BufferedOutputStream(new OutputEncrypt(new FileOutputStream(args[1])));
			
			byte[] buffer = new byte[8*1024];
			int count;
			
			while(( count =in.read(buffer)) > 0) {
				out.write(buffer, 0, count);
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
