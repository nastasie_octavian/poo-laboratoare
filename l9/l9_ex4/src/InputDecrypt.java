import java.io.*;

public class InputDecrypt extends FilterInputStream {

	protected InputDecrypt(InputStream in) {
		super(in);
	}
	
	public int read() throws IOException {
		int data = super.read();
		if(data == -1)
			return -1;
		return data-1;
	}
	
	public int read(byte[] b) throws IOException {
		int result = super.read(b);
		for(int i = 0; i < result;i++)
			b[i]--;
		return result;
	}
	
	public int read(byte[] b, int offset, int len) throws IOException {
		int result = super.read(b, offset, len);
		for(int i = 0; i < result;i++)
			b[i]--;
		return result;
	}
}
