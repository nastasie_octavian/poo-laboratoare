import java.io.*;

public class Ls {

	public static void main(String[] args) {
		if(args.length != 1)
			throw new RuntimeException("Argumente insuficiente!");
		
		File path = new File(args[0]);
		if(path.isFile())
			System.out.println(path.getName() + " "+ path.length());
		else {
			long size = 0;
			
			for(String s : path.list())
				size += recSize(path.getPath() + "/" + s);
			
			System.out.println(path.getName() + " " + size);
		}
	}
	
	public static long recSize(String current) {
		File path = new File(current);
		if(path.isFile())
			return path.length();
		long size = 0;
		try {
			for(String s : path.list())
				size += recSize(path.getPath() +"/" +s);
		} catch(Exception e) {
			System.out.println(path);
			e.printStackTrace();
			return 0;
		}
		return size;
	}
}
