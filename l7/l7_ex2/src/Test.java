import java.util.*;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<Student> students = new HashSet<Student>();
		students.add(new Student("Gigi", 10));
		
		Student ion = new Student("Ion", 9);
		
		if(!students.add(new Student("Gigi", 10)))
			System.out.println("Gigi e duplicat");
		students.add(ion);
		if(!students.add(ion))
			System.out.println("Ion e duplicat");
		
		System.out.println("-------------");
		Iterator<Student> it =  students.iterator();
		while(it.hasNext())
			System.out.println(it.next().nume);
	}

}
