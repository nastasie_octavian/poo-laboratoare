import java.util.*;

public class Student implements Comparable<Object> {

	String nume;
	float medie;
	
	public Student(String nume, float medie) {
		this.nume = nume;
		this.medie = medie;
	}
	
	@Override
	public boolean equals(Object o) {
		Student s = (Student)o;
		return (nume.compareTo(s.nume) == 0) && (medie == s.medie);
		
	}
	
	public boolean equals(Student s) {
		return false;
	}
	
	@Override
	public int hashCode() {
		return nume.hashCode();
	}

	// Metoda din interfata Comparable, pt sortarea alfabetica
	@Override
	public int compareTo(Object o) {
		return nume.compareTo(((Student)o).nume);
	}
}

// Comparator pt TreeMap, pt nota studentului
class StudentComparatorMap implements Comparator<Integer> {
	@Override
	public int compare(Integer o1, Integer o2) {
		return o2 - o1;
	}
}