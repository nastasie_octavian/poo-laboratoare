import java.util.*;

public class StudentMap extends TreeMap<Integer,List<Student>> {
	
	// In constructorul implicit pt clasa mea apelez
	// constructorul pt TreeMap cu Comparatorul pt medie
	public StudentMap() {
		super(new StudentComparatorMap());
	}
	
	
	public void add(Student s) {
		List<Student> ls = this.get(Math.round(s.medie)); // obtin lista in care trebuie sa adaug
		if(ls == null) { // daca nu exista, o creez si o adaug la TreeMap
			ls = new LinkedList<Student>();
			this.put(Math.round(s.medie), ls);
		}
		ls.add(s); // adaug un nou student in lista
	}
}
