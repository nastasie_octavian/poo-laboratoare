import java.util.*;
import java.util.Map.Entry;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// intantierea si adaugarea la map de noi studenti
		StudentMap map = new StudentMap();
		
		map.add(new Student("Gigi1", 9.9F));
		map.add(new Student("Gigi", 9.8F));
		map.add(new Student("Gigi2", 9.8F));
		map.add(new Student("Ana", 8.9F));
		map.add(new Student("Dincu", 5F));
		map.add(new Student("Tarski", 5.49F));
		
		
		for(Entry<Integer, List<Student>> it:  map.entrySet()) {
			List<Student> ls = it.getValue(); // pt fiecare intrare din TreeMap (lista cu elevi)
			
			Collections.sort(ls); // o sortez alfabetic
			
			// afisez studentii cu for-each
			System.out.print(it.getKey() + " [ ");
			for(Student s : ls.toArray(new Student[0])) {
				System.out.print(s.nume + " "+ s.medie + "  ");
			}
			System.out.println("] ");
		}
		
	}
}
