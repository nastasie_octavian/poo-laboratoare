
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("HashSet");
		MyHashSet myhs1 = new MyHashSet();
		for(int i = 1; i < 10; i++)
			myhs1.add(i);
		System.out.println(myhs1);
		
		MyHashSet myhs2 = new MyHashSet();
		for(int i = 5; i < 14; i++)
			myhs2.add(i);
		System.out.println(myhs2);
		
		myhs1.addAll(myhs2);
		System.out.println(myhs1);
		
		System.out.println("LinkedList");
		MyListSet myls1 = new MyListSet();
		for(int i = 1; i < 10; i++)
			myls1.add(i);
		System.out.println(myls1);
		
		MyListSet myls2 = new MyListSet();
		for(int i = 5; i < 14; i++)
			myls2.add(i);
		System.out.println(myls2);
		
		myls1.addAll(myls2);
		System.out.println(myls1);
		


	}

}
