import java.util.*;


public class MyHashSet extends HashSet<Integer> {
	int count = 0;
	
	@Override
	public boolean add(Integer i) {
		boolean ok = super.add(i);
		if(ok)
			count++;
		return ok;
	}
	
	@Override
	public boolean addAll(Collection<? extends Integer> shi) {
		boolean ok = false;
		for(Integer i: shi.toArray(new Integer[0]))
			ok = add(i);
		return ok;
	}
	
	public String toString() {
		String aux = count + " [ ";
		for(Iterator<Integer> it = this.iterator(); it.hasNext(); )
				aux+= it.next()+ " ";
		aux+="]";
		return aux;
	}
}
