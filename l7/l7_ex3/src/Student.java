
public class Student {

	String nume;
	float medie;
	
	public Student(String nume, float medie) {
		this.nume = nume;
		this.medie = medie;
	}
	
	@Override
	public boolean equals(Object o) {
		Student s = (Student)o;
		return (nume.compareTo(s.nume) == 0) && (medie == s.medie);
		
	}
	
	public boolean equals(Student s) {
		return false;
	}
	
	@Override
	public int hashCode() {
		return nume.hashCode();
	}
}
