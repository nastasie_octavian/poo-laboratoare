import java.util.*;

public class StressTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Random rnd = new Random();
		int count = 10000;
		int scale =  2;
		ArrayList<Integer> al = new ArrayList<Integer>();
		LinkedList<Integer> ll = new LinkedList<Integer>();
		
		System.out.println("Adaugare--------");
		long start = System.nanoTime();
		for(int i = 0; i < count; i++)
			al.add(rnd.nextInt());
		long rez = System.nanoTime() - start;
		System.out.println("ArrayList:  " + rez);
		
		start = System.nanoTime();
		for(int i = 0; i < count; i++)
			ll.add(rnd.nextInt());
		rez = System.nanoTime() - start;
		System.out.println("LinkedList: " + rez);
		
		System.out.println("Inserare--------");
		start = System.nanoTime();
		for(int i = 0; i < count/scale; i++)
			al.add(rnd.nextInt(al.size()), rnd.nextInt());
		rez = System.nanoTime() - start;
		System.out.println("ArrayList:  " + rez);
		
		start = System.nanoTime();
		for(int i = 0; i < count/scale; i++)
			ll.add(rnd.nextInt(ll.size()), rnd.nextInt());
		rez = System.nanoTime() - start;
		System.out.println("LinkedList: " + rez);
		
		System.out.println("Stergere--------");
		start = System.nanoTime();
		for(int i = 0; i < count/scale; i++)
			al.remove(rnd.nextInt(al.size()));
		rez = System.nanoTime() - start;
		System.out.println("ArrayList:  " + rez);
		
		start = System.nanoTime();
		for(int i = 0; i < count/scale; i++)
			ll.remove(rnd.nextInt(ll.size()));
		rez = System.nanoTime() - start;
		System.out.println("LinkedList: " + rez);
	}

}
