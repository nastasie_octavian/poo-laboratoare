
public class TestContainer {
	public static void main(String[] args) {
		/* Creez un obiect de tip Factory si creez
		 * un Container de tip Queue si unul de tip Stack*/
		IFactory f = new Factory();
		Container q = f.getContainer(Strategy.FIFO);
		Container s = f.getContainer(Strategy.LIFO);
		/* Manipulez Stack si Queue prin intermediul
		 * interfetei Container si obiectele Task prin 
		 * intermediul interfetei implementate de Task*/
		for(int i = 0; i <5; i++) {
			q.push(new Task1("Task1 " + i));
			s.push(new Task1("Task1 " + i));
		}
		System.out.println("Coada");
		for(int i = 0; i <5; i++) {
			q.pop().execute();
		}
		System.out.println("Stiva");
		for(int i = 0; i <5; i++) {
			s.pop().execute();
		}
	}
}
