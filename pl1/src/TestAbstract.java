
public class TestAbstract {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* creez un vector de clase AbstractTaskRunner 
		 * si instantiez obiecte din clasele derivate */
		AbstractTaskRunner[] runtasks = { new CountTaskRunner(Strategy.FIFO),
				new PrintTaskRunner(Strategy.FIFO), 
				new PrintTaskRunnerDelayd(Strategy.FIFO, 1000),
				new RedoTaskRunner(Strategy.FIFO)};
		
		/* manipulez diferitele clase AbstractTaskRunner si 
		 * Task prin intermediul interfetelor */
		for(int i = 0; i < runtasks.length; i++) {
			int j = 0;
			runtasks[i].addTask(new Task1("Task1 " + i + "" + j++));
			runtasks[i].addTask(new Task1("Task1 " + i + "" + j));
			runtasks[i].addTask(new Task3());
		}
		for(int i = 0; i < runtasks.length; i++)
			runtasks[i].executeAll();
		/* downcast ca sa re execut toate metodele 
		 * deja executate in RedoTaskRunner */
		((RedoTaskRunner)runtasks[3]).reExecute();
	}

}
