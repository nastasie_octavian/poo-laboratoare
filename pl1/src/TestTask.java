
public class TestTask {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* Task find o interfata nu poate fi instantiat
		 * dar poate fi folosit ca tip pt referinta 
		 * atata timp cat intstantiez o clasa ce 
		 * implementeaza interfata Task*/
		Task t1 = new Task1("Dincu brrr");
		Task t2 = new Task2();
		Task t3 = new Task3();
		/* Execut metoda din interfata Task, execute()
		 * pt fiecare clasa care implementeaza interfata Task */
		t1.execute();
		t2.execute();
		t3.execute();
		t3.execute();
		Task t32 = new Task3();
		t32.execute();
	}
}
