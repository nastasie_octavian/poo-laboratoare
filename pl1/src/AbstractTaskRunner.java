/* O clasa abstracta care are definite si implementate
 * metode pentru a stoca si rula mai multe Task-uri*/
public abstract class AbstractTaskRunner {
	/* Un Container in care vor fi memorate clasele
	 * care implementeaza interfata Task */
	protected Container c;
	
	/* Constructorul pentru clasa, primeste Strategia 
	 * de memorare in Container si foloseste un obiect
	 * de tip Factory pentru a creea Containerul 
	 * LIFO sau FIFO */
	public AbstractTaskRunner(Strategy s) {
		Factory f = new Factory();
		c = f.getContainer(s);
	}

	public void addTask(Task t) {
		c.push(t);
	}
	/* Executa fiecare Task din Container si metoda action() */
	public void executeAll() {
		while(!c.isEmpty()) {
			c.pop().execute();
			action();
		}
	}
	/*O metoda abstracta care trebuie definita in
	 * fiecare calsa care mosteneste clasa abstracta*/
	protected abstract void action();
}
