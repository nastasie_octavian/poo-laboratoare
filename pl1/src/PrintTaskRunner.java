import java.util.*;

public class PrintTaskRunner extends AbstractTaskRunner {

	/* Constructorul pt PrintTaskRunner trebuie 
	 * apelat constructorul pt clasa de baza 
	 * deoarece nu are constructor implicit*/
	PrintTaskRunner(Strategy s) {
		super(s);
	}

	/* definesc metoda action */
	@Override
	protected void action() {
		// TODO Auto-generated method stub
		System.out.println(new Date());
	}
}
