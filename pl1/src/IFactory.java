
public interface IFactory {
	Container getContainer(Strategy type);
}
