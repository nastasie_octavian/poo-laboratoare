
public class RedoTaskRunner extends AbstractTaskRunner {
	/* un container care va memora Taskurile executate */
	Container redo;
	/* constructorul pt clasa, apeleaza constructorul pt
	 * clasa de baza si creaza un nou container de tip Stack
	 * pentru a memora Taskurile executate in ordinea inversa*/
	public RedoTaskRunner(Strategy s) {
		super(s);
		IFactory f = new Factory();
		redo = f.getContainer(Strategy.LIFO);
	}

	/* redefinesc metoda de executeAll pentru a memora
	 * Task-urile care sunt executate */
	@Override
	public void executeAll() {
		while(!c.isEmpty()) {
			Task t = c.pop(); 
			t.execute();
			redo.push(t);
			action();
		}
	}
	/* implementez metoda action() */
	@Override
	protected void action() {
		// TODO Auto-generated method stub
		
	}
	
	/* o metoda care executa in ordinea inversa 
	 * Task-urile executate */
	public void reExecute() {
		while(!redo.isEmpty())
			redo.pop().execute();
	}

}
