
public class CountTaskRunner extends AbstractTaskRunner {
	private int count = 0;
	
	public CountTaskRunner(Strategy s) {
		super(s);
	}

	@Override
	protected void action() {
		// TODO Auto-generated method stub
		System.out.println("S-au executat: " + ++count + " taskuri.");
	}
}
