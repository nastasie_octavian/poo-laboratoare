import java.util.*;

/* Implementez o coda (LIFO) pentru toate clasele 
 * care implementeaza interfata Task */
public class Stack implements Container {
	private ArrayList<Task> tasks = new ArrayList<Task>();
	
	/* pop() extrage un element de la sfarsitul ArrayList */
	@Override
	public Task pop() {
		// TODO Auto-generated method stub
		if(tasks.size() != 0) {
			Task aux = tasks.get(tasks.size()-1);
			tasks.remove(tasks.size()-1);
			return aux;
		}
		return null;
	}

	/* push() adauga un element la sfarsitul ArrayList */
	@Override
	public void push(Task task) {
		// TODO Auto-generated method stub
		tasks.add(task);	
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tasks.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tasks.isEmpty();
	}

	@Override
	public void transferFrom(Container container) {
		// TODO Auto-generated method stub
		while(!container.isEmpty())
			tasks.add(container.pop());
	}
}
