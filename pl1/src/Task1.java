/* Clasa Task1 implementeaza interfata Task
 * adica va defini metoda void execute() 
 * declarata in interfata Task */
public class Task1 implements Task {
	private String msg;

	public Task1(String msg) {
		this.msg = msg;
	}
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println(msg);
	}
}
