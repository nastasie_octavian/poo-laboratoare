/* O clasa ce implementeaza interfata IFactory si creeaza 
 * un Container Stack sau Queue in functie de strategia 
 * aleasa Coada - Queue(FIFO) sau Stiva - Stack(LIFO) */
public class Factory implements IFactory {

	@Override
	public Container getContainer(Strategy type) {
		// TODO Auto-generated method stub
		if(type == Strategy.FIFO)
			return new Queue();
		return new Stack();
	}
}
