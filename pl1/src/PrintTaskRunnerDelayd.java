import java.util.Date;


public class PrintTaskRunnerDelayd extends PrintTaskRunner {
	private int delay;
	
	PrintTaskRunnerDelayd(Strategy s, int delay) {
		super(s);
		// TODO Auto-generated constructor stub
		this.delay = delay;
	}

	protected void action() {
		try {
			Thread.sleep(delay);
		}
		catch(Exception e) {
		
		}
		System.out.println(new Date());
	}
}
