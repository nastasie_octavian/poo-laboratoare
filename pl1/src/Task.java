/* O interfata comuna pt toate clasele care
 * vor implementa Task */
public interface Task {

	/**
	 * Executa actiunea caracteristica task-ului
	 */
	void execute();
	
}

/* Orice clasa care implementeaza interfata Task
 * poate fi folosita in orice situatie unde este
 * folosita interfata Task */
