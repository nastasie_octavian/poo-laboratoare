import java.util.*;

/* Implementez o coda (FIFO) pentru toate clasele 
 * care implementeaza interfata Task */
public class Queue implements Container {
	/*Folosesc un ArrayList ca mecanism de stocare pt Task-uri*/
	private ArrayList<Task> tasks = new ArrayList<Task>(); 
	
	/*Controlez si definesc politica de acces la ArryList
	 * prin intermediul metodelor pop() si push() */
	
	/* pop() extrage primul element din ArrayList */
	@Override
	public Task pop() {
		// TODO Auto-generated method stub
		if(tasks.size() > 0) {
			Task aux = tasks.get(0);
			tasks.remove(0);
			return aux;
		}
		else
			return null;
	} 

	/* push() adauga un element la sfarsitul ArrayList */
	@Override
	public void push(Task task) {
		// TODO Auto-generated method stub
		tasks.add(task);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tasks.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return tasks.isEmpty();
	}

	@Override
	public void transferFrom(Container container) {
		// TODO Auto-generated method stub
		while(!container.isEmpty())
			tasks.add(container.pop());
	}
}
