
public class Stiva  extends Array {
	private int poz = 0;
	
	public Stiva(int n) {
		super(n);
	}
	
	public int push(int val) {
		int rez = super.set(poz, val);
		if(rez != ERROR)
			poz++;
		return rez;
	}
	
	public int pop() {
		int rez = super.get(--poz);
		return rez;
	}
}
