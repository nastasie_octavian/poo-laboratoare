
public interface Iterator {
		public boolean hasNext();
		public int next() throws ExcAlt, ExcEnd;
}
