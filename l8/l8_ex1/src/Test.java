import java.util.*;

public class Test {

	/**
	 * @param args
	 */
	static public void f() {
		f();
	}
	public static void main(String[] args) {
		
		MyArrayList list = new MyArrayList(10);
		Random rand = new Random();
		for(int i = 0; i < 5; i++)
			list.add(1 + rand.nextInt(100));
		System.out.println(list);
		Iterator iter = list.iterator();
		try 
		{
			while(iter.hasNext())
				System.out.println(iter.next());
			System.out.println(iter.next());
		}
		catch(ExcEnd ex) {
			ex.msg();
		}
		catch(ExcAlt ex) {
			ex.msg();
		}
		
		list = new MyArrayList(10);
		for(int i = 0; i < 5; i++)
			list.add(1 + rand.nextInt());
		
		iter = list.iterator();
		list.add(13);
		try {
			System.out.println(iter.next());
		}
		catch(ExcEnd ex) {
			ex.msg();
		}
		catch(ExcAlt ex) {
			ex.msg();
		}
		
		
		LinkedList<HashMap<Integer, Integer>> list2 = new LinkedList<HashMap<Integer, Integer>>();
		try {
			while(true) {
				list2.add(new HashMap<Integer, Integer>());
			}
		}
		catch(OutOfMemoryError err) {
				list2.clear();
				System.out.println("Out of memory");
				
		}
		try {
			f();
		}
		catch(StackOverflowError err) {
			System.out.println("Stack overflow error!");
		}
	}

}
