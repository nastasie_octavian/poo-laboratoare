class ExcAlt extends Exception {
	public void msg() {
		System.out.println("S-a modificat lista!");
	}
}

class ExcEnd extends Exception {
	public void msg() {
		System.out.println("Am ajuns la sfarsit!");
	}
}

public class MyArrayList {
	
	private class IteratorMy implements Iterator {

		private int poz = 0;
		@Override
		
		public boolean hasNext() {
			return poz < filled;
		}

		@Override
		public int next() throws ExcAlt, ExcEnd {
			if(modifAfterIter)
				throw new ExcAlt();
			if(poz == filled)
				throw new ExcEnd();
			return vals[poz++];
		}
		
	}
	
	public Iterator iterator() {
		modifAfterIter = false;
		return new IteratorMy();
	}
	
	int[] vals;
	int filled = 0;
	private boolean modifAfterIter;
	
	public MyArrayList() {
		vals = new int[10];
	}
	
	public MyArrayList(int size) {
		vals = new int[size];
	}
	
	public void add(int value) {
		modifAfterIter = true;
		if(filled >= vals.length) {
			int[] aux = new int[2*vals.length];
			for(int i = 0; i < vals.length; i++)
					aux[i] = vals[i];
			vals = aux;
		}
		vals[filled++] = value;
	}
	
	public boolean contains(int value) {
		for(int i = 0; i < filled; i++)
			if(vals[i] == value)
				return true;
		return false;
	}
	
	public void remove(int index) {
		if(index < 0 || index >= filled)
			System.out.println("Eroare !!! Out of bounds!");
		else {
			modifAfterIter = true;
			for(int i = index; i < filled-1; i++)
				vals[i] = vals[i+1]; 
			filled--;
		}
	}
	
	public float get(int index) {
		if(index < 0 || index >= filled) {
			System.out.println("Eroare !!! Out of bounds!");
			return -1;
		}
		else
			return vals[index];
	}
	
	public int size() {
		return filled;
	}
	
	public String toString() {
		String s = new String("[ ");
		for(int i=0; i < filled; i++)
			s+= " " + String.valueOf(vals[i]);
		s+= " ]";
		return s;
	}
}
