import java.util.*;

class OverflowException extends Exception {
	public void msg() {
		System.out.println("Suma este mai mare ca " + Integer.MAX_VALUE );
	}
}

class UnderflowException extends Exception {
	public void msg() {
		System.out.println("Suma este mai mica ca " + Integer.MIN_VALUE );
	}
}

class DivideByZero extends Exception {
	public void msg() {
		System.out.println("Inpartire la 0");
	}
}

class Calculator {
	
	static Integer add(Integer a, Integer b) throws OverflowException, UnderflowException {
		if(Integer.MAX_VALUE - a < b)
			throw new OverflowException();
		if(a < 0 && b < 0)
			if(Integer.MIN_VALUE + (-1*a) > b)
				throw new UnderflowException();
		return a + b;
	}
	static Integer divide(Integer a, Integer b) throws DivideByZero {
		if(b == 0)
			throw new DivideByZero();
		return a / b;
	}
	
	static Integer average(Collection<Integer> coll) throws OverflowException, UnderflowException {
		Integer sum = 0;
		Iterator<Integer> it = coll.iterator();
		while(it.hasNext()) {
			sum = add(sum, it.next());
		}
		try {
			return divide(sum, coll.size());
		} catch (DivideByZero e) {
			return 0;
		}
	}
}


public class Test {

	static int f(int x) {
		try {
			if(x  < 0)
				throw new Exception();
			return x;
		}
		catch(Exception e) {
			return 0;
		}
		finally {
			System.out.println("Se executa finally!");
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer a = Integer.MAX_VALUE;
		Integer b = 10;
		try {
			System.out.println("Adunare: " + Calculator.add(a, b));
		} catch (OverflowException e) {
			e.msg();
		} catch (UnderflowException e) {
			e.msg();
		}
		
		a = 20;
		try {
			System.out.println("Adunare: " + Calculator.add(a, b));
		} catch (OverflowException e) {
			e.msg();
		} catch (UnderflowException e) {
			e.msg();
		}
		
		a = -5;
		b = Integer.MIN_VALUE;
		try {
			System.out.println("Adunare: " + Calculator.add(a, b));
		} catch (OverflowException e) {
			e.msg();
		} catch (UnderflowException e) {
			e.msg();
		}
		
		a = 20;
		b = 5;
		try {
			System.out.println("Divide: " + Calculator.divide(a, b));
		} catch (DivideByZero e1) {
			e1.msg();
		}
		b = 0;
		try {
			System.out.println("Divide: " + Calculator.divide(a, b));
		} catch (DivideByZero e1) {
			e1.msg();
		}
		
		Collection<Integer> coll = new ArrayList<Integer>();
		for(int i = 0; i < 10; i++)
				coll.add(i);
		try {
			System.out.println("Medie: " + Calculator.average(coll));
		} catch (OverflowException e) {
			e.msg();
		} catch (UnderflowException e) {
			e.msg();
		}
		System.out.println(f(12));
		System.out.println(f(-12));
	}

}
