public class Poligon {
	private Punct pct[];
	
	public Poligon(int nrp) {
		pct = new Punct[nrp];
	}
	
	public Poligon(float[] pcts) {
		
		this(pcts.length / 2);
		int aux = 0;
		for(int i = 0; i < pcts.length-1; i+=2) {
			pct[aux++] =  new Punct(pcts[i], pcts[i+1]);
		}
	}
	
	public void show() {
		for(int i = 0; i < pct.length; i++)
			pct[i] .show();
	}
}
