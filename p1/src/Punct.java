public class Punct {
	float x, y;
	
	public Punct(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void changeCoords(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void show() {
		System.out.println("("+ x + ", " + y + ")");
	}
}
