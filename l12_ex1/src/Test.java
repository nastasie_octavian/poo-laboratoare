import java.util.*;

public class Test {
	public static void main(String[] args) {
		Random rand = new Random();
		int[] aux = new  int[100000];
		for(int i = 0; i <100000; i++)
			aux[i] = rand.nextInt();
		
		List notpara = new LinkedList();
		List<Integer> para = new LinkedList<>(); 
		long start = System.nanoTime();
		for(int i = 0; i < aux.length; i++)
			notpara.add(aux[i]);
		System.out.println("NotParametric add: " + (System.nanoTime() - start));
		
		start = System.nanoTime();
		for(int i = 0; i < aux.length; i++)
			para.add(aux[i]);
		System.out.println("   Parametric add: " + (System.nanoTime() - start));
		
		start = System.nanoTime();
		int a;
		for(int i = 0; i < aux.length; i++)
			a = (int)notpara.get(i);
		System.out.println("NotParametric access: " + (System.nanoTime() - start));
		
		start = System.nanoTime();
		for(int i = 0; i < aux.length; i++)
			a = para.get(i);
		System.out.println("   Parametric access: " + (System.nanoTime() - start));
	}
}
