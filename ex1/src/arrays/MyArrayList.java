package arrays;

public class MyArrayList {
	float[] vals;
	int filled = 0;
	
	public MyArrayList() {
		vals = new float[10];
	}
	
	public MyArrayList(int size) {
		vals = new float[size];
	}
	
	public void add(float value) {
		if(filled >= vals.length) {
			float[] aux = new float[2*vals.length];
			for(int i = 0; i < vals.length; i++)
					aux[i] = vals[i];
			vals = aux;
		}
		vals[filled++] = value;
	}
	
	public boolean contains(float value) {
		for(int i = 0; i < filled; i++)
			if(vals[i] == value)
				return true;
		return false;
	}
	
	public void remove(int index) {
		if(index < 0 || index >= filled)
			System.out.println("Eroare !!! Out of bounds!");
		else {
			for(int i = index; i < filled-1; i++)
				vals[i] = vals[i+1]; 
			filled--;
		}
	}
	
	public float get(int index) {
		if(index < 0 || index >= filled) {
			System.out.println("Eroare !!! Out of bounds!");
			return -1;
		}
		else
			return vals[index];
	}
	
	public int size() {
		return filled;
	}
	
	public String toString() {
		String s = new String("[ ");
		for(int i=0; i < filled; i++)
			s+= " " + String.valueOf(vals[i]);
		s+= " ]";
		return s;
	}
}
