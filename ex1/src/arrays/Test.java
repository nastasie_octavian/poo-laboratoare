package arrays;
import java.util.*;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyArrayList list =  new MyArrayList();
		for(int i = 1; i < 5; i++)
			list.add(i);
		for(int i = 0; i < 4; i++) {
			System.out.println("Poz: " + i + " Value: " + list.get(i));
		}
		
		MyArrayList list2 = new MyArrayList(5);
		Random gen = new Random();
		for(int i = 0; i < 10; i++)
			list2.add(gen.nextFloat());
		System.out.println("Lista 2 initial: " + list2.toString());
		
		for(int i = 0; i < 10; i++) {
			float aux = gen.nextFloat();
			if(list2.contains(aux))
				System.out.println("A fost gasita valoarea "+ aux);
			else
				System.out.println(aux + " nu a fost gasit");
		}
		System.out.println("Lista 2 cautare: " + list2.toString());
		
		for(int i = 0; i < 5; i++) {
			float aux = gen.nextFloat();
			for(int j = 0; j < list2.size(); j++)
				if(list2.get(j) == aux) {
					list2.remove(j);
					break;
				}
		}
		System.out.println("Lista 2 dupa stergere: " + list2.toString());
	}

}
