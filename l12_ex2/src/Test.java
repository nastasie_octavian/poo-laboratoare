import java.util.*;

class PQ<T extends Number> {
	private T[] data;
	private int filled = 0;
	
	public PQ() {
		data = (T[])new Number[100];
	}
	
	private void resize() {
		T[] aux = (T[]) new Number[data.length * 2];
		for(int i = 0; i < filled; i++)
			aux[i] = data[i];
		data = aux;
	}
	
	public void add(T number) {
		int i;
		if(filled == data.length)
			resize();
		for(i  = 0; i < filled && (data[i].doubleValue() < number.doubleValue()); i++);
		
		for(int j = filled; j > i; j--)
			data[j] = data[j-1];
		data[i] = number;
		filled++;
	}
	
	public void copy(Collection<T> c) {
		for(T t : c)
			add(t);
	}
	
	public String toString() {
		return Arrays.toString(data);
	}
}

public class Test {
	public static void main(String[] args) {
		Random rnd = new Random();
		PQ priorityQ = new PQ();
		for(int i = 0; i < 10; i++)
			priorityQ.add(rnd.nextInt(100));
		System.out.println(priorityQ);
		
		List<Integer> afara = new LinkedList<Integer>();
		for(int i = 0; i < 10; i++)
			afara.add(rnd.nextInt(100));
		priorityQ.copy(afara);
		System.out.println(priorityQ);
	}
}
