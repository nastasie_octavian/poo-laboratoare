import java.util.*;

interface Visitor {
    public void visit(Employee e);
    public void visit(Boss b);
}

class RevenueVisitor implements Visitor {

    @Override
    public void visit(Employee e) {
            System.out.println(e.getName() + " " + e.getSalary());
            
    }

    @Override
    public void visit(Boss b) {
            System.out.println(b.getName() + " " + (b.getSalary() + b.getBonus()));
            
    }
    
}

class BonusVisitor implements Visitor {

	private float sum = 0;
	private int count = 0;
	
	public float average() {
		return sum / count;
	}
    @Override
    public void visit(Employee e) {
            count++;
    }

    @Override
    public void visit(Boss b) {
            count++;
            sum += b.getBonusPercentage();
    }
    
}
interface Visitable {
    public void accept(Visitor v);
}

class Employee implements Visitable {
        
        String  name;
        float   salary;
        
        public Employee(String name, float salary) {
                this.name       = name;
                this.salary     = salary;
        }

        public String getName() {
                return name;
        }

        public float getSalary() {
                return salary;
        }
        
        public float getBonusPercentage() {
        	return 0;
        }
        
        @Override
        public void accept(Visitor v) {
                v.visit(this);          
        }
}

class Boss extends Employee {
        
		private LinkedList<Employee> angajati = new LinkedList<Employee>();
        float bonus;

        public Boss(String name, float salary) {
                super(name, salary);
                bonus = 0;
        }
        
        public void addAngajat(Employee angajat) {
        	angajati.add(angajat);
        }
        
        public float getBonus() {
                return bonus;
        }

        public void setBonus(float bonus) {
                this.bonus = bonus;
        }
        
        public float getBonusPercentage() {
        	return 100 * bonus / salary;
        }
        
       @Override
        public void accept(Visitor v) {
                v.visit(this);    
                Iterator<Employee> it = angajati.iterator();
                while(it.hasNext())
                	it.next().accept(v);
        }
}

public class Test {

        public static void main(String[] args) {
                Boss ceo = new Boss("CEO", 1000);
                ceo.setBonus(100);
                
                Boss boss2 = new Boss("boss2", 500);
                boss2.setBonus(50);
                
                boss2.addAngajat( new Employee("ang1", 200));
                boss2.addAngajat(new Employee("ang2", 200));
                
                ceo.addAngajat(boss2);
                Boss boss3 = new Boss("boss3", 500);
                boss3.setBonus(50);
                
                ceo.addAngajat(boss3);
                
                Visitor v = new RevenueVisitor();  
                ceo.accept(v);
                BonusVisitor vb = new BonusVisitor();  
                ceo.accept(vb);
                
	            System.out.println("In medie: " + vb.average()); 
        }
}