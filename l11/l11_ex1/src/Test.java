import java.util.*;

interface Visitor {
    public void visit(Employee e);
    public void visit(Boss b);
}

class RevenueVisitor implements Visitor {

    @Override
    public void visit(Employee e) {
            System.out.println(e.getName() + " " + e.getSalary());
            
    }

    @Override
    public void visit(Boss b) {
            System.out.println(b.getName() + " " + (b.getSalary() + b.getBonus()));
            
    }
    
}


interface Visitable {
    public void accept(Visitor v);
}

class Employee implements Visitable {
        
        String  name;
        float   salary;
        
        public Employee(String name, float salary) {
                this.name       = name;
                this.salary     = salary;
        }

        public String getName() {
                return name;
        }

        public float getSalary() {
                return salary;
        }
        
        @Override
        public void accept(Visitor v) {
                v.visit(this);          
        }
}

class Boss extends Employee {
        
        float bonus;

        public Boss(String name, float salary) {
                super(name, salary);
                bonus = 0;
        }
        
        public float getBonus() {
                return bonus;
        }

        public void setBonus(float bonus) {
                this.bonus = bonus;
        }
        
       @Override
        public void accept(Visitor v) {
                v.visit(this);          
        }
}

public class Test {

        public static void main(String[] args) {
                Boss becali;
                List<Employee> employees = new LinkedList<Employee>();
                
                employees.add(new Employee("Costel", 15));
                employees.add(new Employee("Gigel", 20));
                employees.add(becali = new Boss("Becali", 1000));
                becali.setBonus(100);
                
                Visitor v = new RevenueVisitor();        // init visitor with a concrete visitor class
                for (Employee e : employees)
                        e.accept(v);

        }
}